import React from 'react';
import './Error404.css';

export class Error404 extends React.Component {
    render() {
        return (
            <section id="Error404">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Error404 p, #Error404 li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                <h1>Esta es la página de Error 404</h1>
            </section>
        );
    }
}

import React from 'react';
import Loader from '../components/Loader';
import Api from '../lib/Api';
import './ComicDetails.scss';
import { addComic } from '../actions';
import { connect } from 'react-redux';
import Swal from 'sweetalert2';
class ComicDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentComicID: props.match.params.id,
            disableButton: false,
            isLoading: true,
            comic: {}
        }
        this._addToMyColecction = this._addToMyColecction.bind(this);
    }
    componentDidMount() {
        if (!this.state.comic?.id) {
            Api.getComicByID(this.state.currentComicID).then(response => {
                const comic = response.data?.data?.results[0];
                this.setState({
                    comic,
                    isLoading: false
                });
            }).catch(e => {
                // Si no tenemos respuesta (500, 404) redireccionamos a la 404
                this.props.history.push('/404');
            });
        }
    }
    _addToMyColecction() {
        if (!this.state.disableButton) {
            // Lanzamos la accion del store "addComic"
            this.props.dispatch(addComic({
                id: this.state.comic.id,
                title: this.state.comic.title,
                cover: this.state.comic.thumbnail.path + '.' + this.state.comic.thumbnail.extension
            }));
            this.setState({
                disableButton: true
            });
            // Lanzamos una ventana modal indicando el éxito de la operación
            Swal.fire({
                title: '¡Comic agregado a su colección!',
                text: `Hemos agregado el comic ${this.state.comic.title} a su colección.`,
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        }
    }
    _comicDetails() {
        if (this.state.comic?.id) {
            const aditionalLinks = [];
            if (this.state.comic?.urls && Array.isArray(this.state.comic.urls)) {
                for (let link of this.state.comic.urls) {
                    if (link?.type === 'purchase') {
                        link.text = 'Comprar';
                        aditionalLinks.push(link);
                    } else if (link?.type === 'detail') {
                        link.text = 'Detalles';
                        aditionalLinks.push(link);
                    }
                }
            }
            return (
                <React.Fragment>
                    <div className="col-12 col-md-6">
                        <img src={this.state.comic.thumbnail.path + '.' + this.state.comic.thumbnail.extension} alt={this.state.comic.title} className="img-fluid"/>
                    </div>
                    <div className="col-12 col-md-6">
                        <h1 className="display-4 text-white">{this.state.comic.title}</h1>
                        <div className="actions">
                            <button className={"btn btn_add " + (this.state.disableButton ? 'disabled' : '')} onClick={this._addToMyColecction}>Añadir a mi colección</button>
                            {
                                aditionalLinks.map((link, index) => {
                                    return (
                                        <a href={link.url} className={ "btn btn-" + link.type } key={index} target="_blank" rel="noopener noreferrer">{link.text}</a>
                                    );
                                })
                            }
                        </div>
                        <p className="description text-white">{this.state.comic.description || 'No se ha proporcionado descripción.'}</p>
                    </div>
                </React.Fragment>
            );
        }
    }
    render() {
        return (
            <div id="ComicDetails">
                <Loader isLoading={ this.state.isLoading } />
                {
                    /*
                    * Debido a que cargamos el componente mediante Router, éste inyectará
                    * las propiedades a nuestro componente. En la propiedad props tendremos
                    * la implementación de "history" de React Router, tendremos la propiedad
                    * "location" que nos evolverá la URL actual y, lo más importante, la propiedad
                    * "match" que nos devolverá los parámetros mediante la propiedad "params", 
                    * entre otras muchas propiedades.
                    */
                }
                <div className="container p-5">
                    <div className="row comic_content">
                        {
                            this._comicDetails()
                        }
                    </div>
                </div>
            </div>
        );
    }
}

// Inyectamos el state (comics solo) a nuestro componente
const mapStateToProps = state => ({
    comics: state.comics
});
// Con "connect" podremos conectar el map del state a propiedades con el Componente
export default connect(mapStateToProps)(ComicDetails);


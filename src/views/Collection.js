import React from 'react';
import { connect } from 'react-redux';

import './Collection.css';

/*
 * Nota: exportamos directamente de esta forma (nombrada) para poder
 * importar todo en nuestro fichero views/index.js e importar en nuestra
 * App de forma más sencilla (ver fichero views/index.js para más información).
 *  
*/
class Collection extends React.Component {
    constructor(props) {
        super(props);
        this._renderCollection = this._renderCollection.bind(this);
    }
    _renderCollection() {
        const IDs = Object.keys(this.props.comics.comicCollection);
        if (IDs.length === 0) {
            return (
                <div className="row">
                    <div className="col-12">
                        <h1>No se han encontrado comics</h1>
                        <p className="lead">Agrega un comic primero a tu colección</p>
                    </div>
                </div>
            )
        }
        return Object.keys(this.props.comics.comicCollection).map((id, index) => {
            return (
                <div className="comic" key={index}>
                    <img src={this.props.comics.comicCollection[id].cover} alt=""/>
                    <h1>{this.props.comics.comicCollection[id].title}</h1>
                </div>
            )
        })
    }
    render() {

        return (
            <section id="Collection">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Collection p, #Collection li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                {
                    this._renderCollection()
                }
            </section>
        );
    }
}

// Inyectamos el state (comics solo) a nuestro componente
const mapStateToProps = state => ({
    comics: state.comics
});
// Con "connect" podremos conectar el map del state a propiedades con el Componente
export default connect(mapStateToProps)(Collection);

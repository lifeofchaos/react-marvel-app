/*
 * A través de este fichero podremos exportar todas las vistas e importarlas en nuestra App
 * sin necesidad de replicar la línea "import VISTA from './VISTA.js'".
 * Exportamos aquí todo y así podremos importar de forma cómoda mediante
 * import { VISTA1, VISTA2, VISTAn } from './views';
 * 
*/
export * from './Home.js';
// Como exportamos el componente como default, tendremos que nombrarlo al vuelo
export { default as Collection } from './Collection.js';
export * from './Favourites.js';
export * from './Shared.js';
export * from './Export.js';
export * from './Error404.js';
// Como exportamos el componente como default, tendremos que nombrarlo al vuelo
export { default as ComicDetails } from './ComicDetails.js';
import React from 'react';
import './Export.css';

export class Export extends React.Component {
    render() {
        return (
            <section id="Export">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Export p, #Export li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                <h1>Esta es la página de "Exportar"</h1>
            </section>
        );
    }
}

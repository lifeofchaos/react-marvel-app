import React from 'react';
import { Link } from 'react-router-dom';
// Si, estamos importando un SVG directamente 😮
// Puede ser también cualquier otro fichero de imagen. Lo meteremos como atributo src
import FeaturedImage from '../assets/images/home_product_lead_image_right.svg';
import Api from '../lib/Api';
import ComicHomeItem from '../components/ComicHomeItem';
import './Home.scss';

export class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comics: Array(12).fill([])
        };
    }
    componentDidMount() {
        if (this.state.comics[0].length === 0) {
            Api.getLastComics().then(({data: response}) => {
                this.setState({
                    comics: response.data.results
                })
            });
        }
    }
    render() {
        return (
            <section id="Home">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Home p, #Home li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                <div className="container">
                    <div className="row p-0 m-0 align-items-center">
                        <div className="col-12 col-md-6">
                            <p className="display-2 custom_text_size_display">Mantén tu colección en orden</p>
                            <p className="lead custom_text_size_lead">Gracias a nuestra plataforma podrás obtener información acerca de tus comics favoritos, localizar rápidamente tus comics destacados y compartir fácilmente tu colección con tus amigos.</p>
                            <Link to="/coleccion" className="btn">Ir a mi colección</Link>
                        </div>
                        <div className="col-12 col-md-6">
                            <img src={FeaturedImage} className="img-fluid" alt="Mantén tu colección en orden" />
                        </div>
                    </div>
                </div>
                <div className="container-fluid lastest_comics">
                    <h2 className="latest_section">Últimas publicaciones</h2>
                    <div className="container">
                        <div className="row comic_grid">
                            {
                                this.state.comics.map((comic, index) => {
                                    return <ComicHomeItem comic={comic} key={index} />
                                })
                            }
                        </div>
                        <div className="row justify-content-center view_all_cta">
                            <Link className="btn" to="/coleccion">Mostrar todos</Link>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

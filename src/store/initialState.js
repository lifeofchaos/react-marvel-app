// Este será el estado inicial del store: contiene la estructura base
// y el contenido está vacío.

const initialState = {
    comics: {
        comicCollection: {}
    }
};

export default initialState;
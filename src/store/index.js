import { createStore } from 'redux';
import reducer from '../reducers';
import { loadState } from './localStorageSupport';

// Exportaremos la función "initStore"
// que invocará a "createStore" de redux,
// necesario para indicar que eso es un store
// de redux

export default function initStore() {
    // Agregamos "LoadState" para cargar nuestro state del Local Storage
    return createStore(reducer, loadState());
}
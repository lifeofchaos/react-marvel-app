import { combineReducers } from 'redux';
import comicCollectionReducer from './comicCollectionReducer';

// combineReducers nos permite combinar los reducers
// por cada propiedad del store
// Nosotros trabajaremos con "comics" por lo que los
// reducers son sobre esta propiedad
const reducer = combineReducers({
    comics: comicCollectionReducer
});

export default reducer;

import React from 'react';
import { BrowserRouter, Switch, Route, NavLink, Redirect } from 'react-router-dom';
import { Home, Collection, Favourites, Shared, Export, Error404, ComicDetails } from './views';
import './App.scss';

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        {
         /* 
         * Todo el contenido de nuestra App estará englobado bajo el componente BrowserRouter.
         * Este componente escuchará todos los cambios sobre la URL y mediante el componente
         * Switch modificará la vista correspondiente.
         * 
         * Nota: en algunos tutoriales BrowserRouter lo importan como Router mediante:
         * import { BrowserRouter as Router } from 'react-router-dom'
         * y posteriormente el tag BrowserRouter lo reemplazan por Router 
         * (pues se ha importado con ese nombre mediante "as").
         */
        }
        <header>
          {
           /* 
           * El HTML tag header lo emplearemos para englobar nuestra cabecera: logo y menú
           */
          }
          {
           /* 
           * Agregamos clases CSS definidas por Boostrap como, por ejemplo, navbar, nav-item...
           * Como podéis observar, hay una diferencia notable entre HTML y JSX y es que las clases
           * CSS se deben agregar con "className" pues class es una palabra reservada de ES6+.
           * 
           * Con esto ya tenemos unos ajustes mínimos del menú (salvo el responsive, tendremos que trabajar
           * qué sucede con el botón de mostrar menú cuando el menú está oculto).
           */
          }
          <nav className="navbar navbar-expand-lg">
            {
             /* 
              * Vamos a crear un menú sin estilos a modo ejemplo para ver cómo crear enlaces
              * mediante React Link, estos "enlaces" serán manejados por React Router DOM
              * y nos permitirán modificar la vista (página) sin que el usuario abandone el 
              * sitio web (emplea la API de HTML5 History para manipular la URL y a su vez el DOM)
             */
            }
            <NavLink to="/" className="navbar-brand">App Logo</NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu_principal" aria-controls="menu_principal" aria-expanded="false" aria-label="Ver/Ocultar Menú">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="menu_principal">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <NavLink exact to="/" className="nav-link" activeClassName="currentItem">Inicio</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink strict to="/coleccion" className="nav-link" activeClassName="currentItem">Mi Colección</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink strict to="/favoritos" className="nav-link" activeClassName="currentItem">Mis favoritos</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink strict to="/compartidos" className="nav-link" activeClassName="currentItem">Colecciones compartidas</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink strict to="/exportar" className="nav-link" activeClassName="currentItem">Exportar</NavLink>
                </li>
                <li className="nav-item">
                  {
                    /*
                    * A modo ejemplo, vamos a crear una URL que no existe para crear el
                    * comportamiento de un 404, como lo haríamos en cualquier aplicación
                    * si la URL no existe.
                    */
                  }
                  <NavLink strict to="/sdasdsaf" className="nav-link" activeClassName="currentItem">Forzar Error 404</NavLink>
                </li>
              </ul>
            </div>
          </nav>
        </header>
        <main className="container-fluid p-0 m-0">
          {
            /*
             * Con el HTML tag "main" estamos definiendo el contenido principal de la web
            */
          }
          <Switch>
            {
              /*
               * El componente Switch revisará la URL (los casos de coincidencia se especifican
               * mediante el uso de Route) y, dependiendo de la URL, modificará el contenido al
               * caso de coincidencia (ejecutará el método render del componente o mostrará el HTML
               * englobado en dicho caso).
              */
            }
            {
              /*
               * Cargamos el componente "Home" (actúa de vista con su propia lógica) como atributo.
               * 
               * El atributo "exact" le indica a Route que debe coincidir exactamente
               * con "/" la URL, si no se pone este atributo, todas las URL coincidirán con "/"
               * pues la expresión regular no buscará coincidencia exacta.
              */
            }
            <Route exact path="/" component={Home} />
            {
              /*
               * Cargamos el componente "Collection" (actúa de vista con su propia lógica) como atributo.
              */
            }    
            <Route path="/coleccion" component={Collection} />
            {
                /*
                 * Cargamos el componente "Favourites" (actúa de vista con su propia lógica) como atributo.
                */
            }            
            <Route path="/favoritos" component={Favourites} />
            {
              /*
               * Cargamos el componente "Shared" (actúa de vista con su propia lógica) como atributo.
              */
            }
            <Route path="/compartidos" component={Shared} />
            {
              /*
               * Cargamos el componente "Export" (actúa de vista con su propia lógica)
              */
            }      
            <Route path="/exportar" component={Export} />
            {
              /*
               * Cargamos el componente "Error404" (actúa de vista con su propia lógica) como atributo.
              */
            }
            <Route path="/404" component={Error404} />
            {
                /*
                 * Para agregar un parámetro en la URL, tendremos que agregar ":nombre", siendo
                 * nombre el nombre del parámetro. Si quiremos validar mediante expresiones regulares
                 * este parámetro (en este caso que sea numérico), agregaremos la expresion regular
                 * entre paréntesis acompañando a la variable: "(\d+)" indica que debe ser numérico.
                 * 
                 * Ahora vamos a cargar el componente (vista) ComicDetails mediante Route.
                 * Para ello le indicaremos mediante el atributo "component" el componente
                 * que queremos cargar. Al hacer esto, toda la lógica de parámetros, etc, se agregarán
                 * como propiedades de nuestro componente.
                */
              }
            <Route exact path="/comic/:id(\d+)" component={ComicDetails} />
            <Route path="*">
              {
                /*
                 * Path * indica cualquier ruta. Deberá colarse al final pues si se hace
                 * antes todas las páginas caerán en este caso pues busca en el orden
                 * que se define aquí.
                 * Mediante "Redirect" estaremos redireccionando al usuario (modificando la URL)
                 * a la página "/404" (usando la propiedad "to").
                */
              }
              <Redirect to="/404" />
            </Route>
          </Switch>
        </main>
      </BrowserRouter>
    );
  }
}

export default App;

import React from 'react';
import './Shared.css';

export class Shared extends React.Component {
    render() {
        return (
            <section id="Shared">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Shared p, #Shared li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                <h1>Esta es la página de "Colecciones Compartidas"</h1>
            </section>
        );
    }
}

// Utilizaremos esta función para cargar el Store del Local Storage
export const loadState = () => {
    try {
        const storedData = localStorage.getItem('comic_data');
        if (storedData === null) {
            // Si no existe el state en el local storage devolvemos undefined para cargar el state por defecto
            return undefined; 
        }
        // Como guardaremos como string un JSON, debemos parsearlo antes
        return JSON.parse(storedData);
    } catch (error) {
        return undefined; // Si ocurre algun error, devuelve undefined para cargar el state inicial.
    }
}

// Emplearemos esta función para actualizar el LocalStorage cada vez que se produzca un cambio en el Store
export const saveState = (state) => {
    try {
        // Necesitamos almacenar un String por lo que usaremos JSON.stringify
        let dataToLocalStorage = JSON.stringify(state);
        // Guardamos el "LocalStorate" el state
        localStorage.setItem('comic_data', dataToLocalStorage);
    } catch (error){
        console.log(error);
     }
}
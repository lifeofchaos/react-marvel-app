import React from 'react';
import './Favourites.css';

export class Favourites extends React.Component {
    render() {
        return (
            <section id="Favourites">
                {
                  /*
                  * Mediante la HTML tag "section" indicaremos la sección,
                  * esta sección tendra como ID una etiqueta que usaremos en
                  * el fichero CSS para personalizar esta vista.
                  * Por ejemplo: #Favourites p, #Favourites li, etc. 
                  * De esta forma evitamos afectar al resto de elementos.
                  */
                }
                <h1>Esta es la página de "Favoritos"</h1>
            </section>
        );
    }
}

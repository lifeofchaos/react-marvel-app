import initialState from '../store/initialState.js';

// Como el state siempre debe devolverse, en caso de estar vacío o no sufrir
// cambios, deberemos devolver el state inicial, por ello state = initialState.comics
export default function comicCollectionReducer(state = initialState.comics, action) {
    // Todas las actions tienen un tipo y este tipo se correspone con un action
    // en caso de no tener un action para manejar esta acción, devolveremos el state
    // sin alterar
    switch (action.type) {
        case 'ADD_COMIC':
            // Queremos construir una colección del tipo: { IDCOMIC: COMIC }
            const comicCollection = Object.assign(state.comicCollection, {[action.payload.id]: action.payload});
            return {
                ...state,
                comicCollection
            }
            default:
                return state;
    }
}
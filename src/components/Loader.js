import React from 'react';
import Boom from '../assets/images/loader_boom.svg';
import Kaboom from '../assets/images/loader_kaboom.svg';
import Pow from '../assets/images/loader_pow.svg';
import Wham from '../assets/images/loader_wham.svg';
import anime from 'animejs';
import './Loader.scss';

class Loader extends React.Component {
    constructor(props) {
        super(props);
        this.image1 = React.createRef();
        this.image2 = React.createRef();
        this.image3 = React.createRef();
        this.image4 = React.createRef();
        this.state = {
            animeTL: anime.timeline({
                loop: true,
                autoplay: false,
                easing: 'easeInOutSine'
            })
        }
    }
    componentDidMount() {
        this.state.animeTL.add({
            targets: this.image1.current,
            rotateZ: '-40',
            translateX: ['0', '50'],
            duration: 0
        })
        .add({
            targets: this.image1.current,
            scale: ['1', '1.2'],
            opacity: ['0', '1'],
            duration: 800
        })
        .add({
            targets: this.image1.current,
            opacity: ['1', '0'],
            scale: ['1.2', '1'],
            duration: 300
        })
        .add({
            targets: this.image2.current,
            rotateZ: '-20',
            translateX: ['0', '-60'],
            duration: 0
        })
        .add({
            targets: this.image2.current,
            opacity: ['0', '1'],
            scale: ['1', '1.4'],
            duration: 800,
        })
        .add({
            targets: this.image2.current,
            opacity: ['1', '0'],
            scale: ['1.4', '1'],
            duration: 300,
        })
        .add({
            targets: this.image3.current,
            rotateZ: '-20',
            translateX: ['0', '-40'],
            duration: 0
        })
        .add({
            targets: this.image3.current,
            opacity: ['0', '1'],
            scale: ['1', '1.5'],
            duration: 800,
        })
        .add({
            targets: this.image3.current,
            opacity: ['1', '0'],
            scale: ['1.5', '1'],
            duration: 300
        })
        .add({
            targets: this.image4.current,
            rotateZ: '20',
            translateX: ['50', '0'],
            duration: 0
        })
        .add({
            targets: this.image4.current,
            opacity: ['0', '1'],
            scale: ['1', '1.3'],
            duration: 800,
        })
        .add({
            targets: this.image4.current,
            opacity: ['1', '0'],
            scale: ['1.3', '1'],
            duration: 300
        })
        ;
        this.state.animeTL.play();
    }
    componentWillUnmount() {
        this.state.animeTL.pause();
    }
    render() {
        if (this.props.isLoading) {
            return (
                <div id="Loader" ref={this.loaderRef}>
                    <img id="image_1" src={Boom} className="loader_image" alt="" ref={this.image1}/>
                    <img id="image_2" src={Kaboom} className="loader_image" alt="" ref={this.image2}/>
                    <img id="image_3" src={Pow} className="loader_image" alt="" ref={this.image3}/>
                    <img id="image_4" src={Wham} className="loader_image" alt="" ref={this.image4}/>
                </div>
            );
        }
        return null;
    }
}

export default Loader;

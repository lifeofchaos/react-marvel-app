import axios from 'axios';

class Api {
    constructor() {
        this.axios = axios.create({
            baseURL: 'https://gateway.marvel.com/v1/public',
            /*
            * Desde la versión 0.18.0, se eliminó "sin querer" la funcionalidad de parámetros
            * por defecto. Estiman resolverla en la versión 0.20.0, por lo que alcanzada esta
            * versión, descomentaremos este fragménto de código y comentaremos el método _params()
            params: {
                apikey: '666988a2c6dfb07e88e9485338a3ca7b',
                ts: 1,
                hash: 'a23b9bbb8d20e56b2f7e54efae83a429', // md5 of ts(1) + privateKey + publicKey
            }
            */
        });
    }
    _params(params) {
        return {
            apikey: '666988a2c6dfb07e88e9485338a3ca7b',
            ts: 1,
            hash: 'a23b9bbb8d20e56b2f7e54efae83a429', // md5 of ts(1) + privateKey + publicKey
            ...params,
        }
    }
    getLastComics(offset = 0, limit = 12) {
        // Ver: https://developer.marvel.com/docs#!/public/getComicsCollection_get_6
        return this.axios.get('/comics', {
            params: this._params({
                orderBy: '-focDate',
                limit: limit,
                offset: offset
            })
        });
    }
    getComicByID(comicId) {
        return this.axios.get(`/comics/${comicId}`, {
            params: this._params({
                comicId
            })
        });
    }
}

const api = new Api();

export default api;

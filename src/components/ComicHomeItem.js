import React from 'react';
import { withRouter } from 'react-router-dom';
import './ComicHomeItem.scss';

class ComicHomeItem extends React.Component {
    constructor(props) {
        super(props);
        this._style = this._style.bind(this);
    }
    _style() {
        if (this.props.comic?.thumbnail?.path && this.props.comic?.thumbnail?.extension) {
            return {
                backgroundImage: `url(${this.props.comic.thumbnail.path}.${this.props.comic.thumbnail.extension})`,
                transition: 'all .5s ease-in-out'
            }
        }
        return {};
    }
    _loadComicInfo(comic) {
        if (typeof comic === 'object') {
            /*
             * Invocaremos a la propiedad "history" que nos llega debido a que hemos exportado el componente
             * envuelto gracias a la función "withRouter" que nos brinda esta propiedad.
            */
            this.props.history.push(`/comic/${comic.id}`);
        }
    }
    render() {
        return (
            <div className="col-12 col-sm-6 col-md-4 col-lg-3 comic_item_container">
                <div className={"comic_item " + (this.props.comic?.thumbnail?.path ? "" : "animate-bg")} style={this._style()} onClick={() => {this._loadComicInfo (this.props.comic)}}>
                </div>
            </div>
        );
    }
}
export default withRouter(ComicHomeItem);

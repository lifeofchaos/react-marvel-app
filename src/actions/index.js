/*
 * Las acciones nos van a permitir invocar el reducer
 * para interactuar con el store. Para facilitar su uso,
 * exportaremos función a función los cambios que podremos
 * hacer sobre el store. 
 * Esta función devolverá un objeto action con el type (que
 * se usa para invocar el reducer) y opcionalmente el payload
 * (los datos que se pasan al reducer)
*/ 
export const addComic = (comic) => {
    return {
        type: 'ADD_COMIC',
        payload: comic
    }
};
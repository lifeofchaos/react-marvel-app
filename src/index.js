import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import initStore from './store';
import { Provider } from 'react-redux';
import { saveState } from './store/localStorageSupport';

// Creamos el store
const store = initStore();

// Guardamos todos los cambios a LocalStorage
store.subscribe(() => {
  saveState(store.getState());
});
 
// Para pruebas, lo que vamos a hacer es
// "escuchar" los cambios sobre el store
// e imprimirlos en consola.

store.subscribe(() =>
  console.log(store.getState())
);

ReactDOM.render(
  <Provider store={store}>
    {
      /*
       * El componente Provider nos proporcionará el acceso al store de Redux
       * en cualquier componente dentro de la App.
      */
    }
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
